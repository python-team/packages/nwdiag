blockdiag>=3.0.0

[pdf]
reportlab

[rst]
docutils

[testing]
nose
pep8>=1.3
flake8
flake8-coding
flake8-copyright
flake8-isort
reportlab
docutils
